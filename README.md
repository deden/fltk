# EiffelTk

EiffelTk (le module `fltk`) est une surcouche simplifiée à tkinter pour
l'enseignement. Ce module fournit une API simple permettant de dessiner sur un
canevas unique, et de récupérer divers événements (souris et clavier) par
attente active, sans introduire la notion de callbacks.

## Fonctionnalités futures

Voici quelques exemples de fonctionnalités envisagées pour les prochaines
versions de EiffelTk :

- choix des événements suivis (move, double clic, etc...)
- déterminer l'élément graphique cliqué
- redimensionnement et autres transformations d'images (rotation...)
- son